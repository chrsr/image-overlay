
    function loadImage (src, onload) {
        var img = new Image();
        img.onload = onload;
        img.src = src;
        return img;
    }

    function createCanvas (facebookImage) {
        var canvas = document.getElementById("image");
        var ctx = canvas.getContext("2d");

        var dv = loadImage('darth-vader.jpg', function () {
            ctx.drawImage(dv, 0, 0);
            ctx.globalAlpha = 1;
        });

        var fb = loadImage(facebookImage, function () {
            ctx.drawImage(fb, 213, 72);
            ctx.globalAlpha = 0.5;
        });

        $('#share').show().on('click', function () {
            console.log('share');
            FB.ui({
              method: 'share'
            }, function (response){

            });
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
          appId      : '208419985886386',
          xfbml      : true,
          version    : 'v2.0'
        });
        FB.Event.subscribe('auth.authResponseChange', function (response) {
            if (response.status === 'connected') {
                FB.api("/" + response.authResponse.userID + "/picture?type=normal", function (response) {
                    console.log(response);
                    if (response && !response.error) {
                        createCanvas(response.data.url);
                    }
                });
            } else if (response.status === 'not_authorized') {
                FB.login();
            } else {
                FB.login();
            }
        });
    };

    // Facebook API
    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));